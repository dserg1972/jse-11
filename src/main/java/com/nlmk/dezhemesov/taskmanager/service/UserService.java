package com.nlmk.dezhemesov.taskmanager.service;

import com.nlmk.dezhemesov.taskmanager.entity.User;
import com.nlmk.dezhemesov.taskmanager.enumerate.Role;
import com.nlmk.dezhemesov.taskmanager.repository.UserRepository;

import java.util.List;

/**
 * Служба пользователей
 */
public class UserService {
    /**
     * Репозиторий пользователей
     */
    private UserRepository userRepository;

    /**
     * Конструктор
     *
     * @param userRepository репозиторий пользователей
     */
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Создание учётной записи и помещение в репозиторий
     *
     * @param login имя учетной записи
     * @param password пароль
     * @param role роль
     * @return созданная учётная запись
     * @throws UserRepository.DuplicateUserException в случае обнаружения дублирующей записи
     */
    public User create(String login, String password, Role role) throws UserRepository.DuplicateUserException {
        if (login == null || login.equals(""))
            return null;
        return userRepository.create(login, password == null ? "" : password, role);
    }

    /**
     * Поиск учетной записи по имени
     *
     * @param login имя учётной записи
     * @return учётная запись
     */
    public User find(String login) {
        return userRepository.find(login);
    }

    /**
     * Получение списка учётных записей
     *
     * @return список учётных записей
     */
    public List<User> findAll() {
        return userRepository.findAll();
    }
}
