package com.nlmk.dezhemesov.taskmanager.util;

import com.nlmk.dezhemesov.taskmanager.entity.User;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Класс для работы с хэш-функциями
 */
public class Hash {
    /**
     * Вычисление хэш-кода для строки
     *
     * @param value исходная строка
     * @return хэш-значение
     */
    public static String generateMD5(String value) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            return Base64.getEncoder().encodeToString(messageDigest.digest(value.getBytes()));
        } catch (NoSuchAlgorithmException e) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, "Unknown hash algorithm", e);
        }
        return null;
    }
}
