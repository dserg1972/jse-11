package com.nlmk.dezhemesov.taskmanager.entity;

/**
 * Сущность "Задача"
 */
public class Task {
    /**
     * Идентификатор экземпляра
     */
    private final Long id = System.nanoTime();

    /**
     * Имя задачи
     */
    private String name = "";

    /**
     * Описание задачи
     */
    private String description = "";

    /**
     * Идентификатор проекта, в который входит задача
     */
    private Long projectId = null;

    /**
     * Имя связанного проекта
     */
    private String projectName = null;

    /**
     * Конструктор по умолчанию
     */
    public Task() {

    }

    /**
     * Конструктор с заданием имени задачи
     *
     * @param name имя задачи
     */
    public Task(String name) {
        this.name = name;
    }

    /**
     * Конструктор с заданием имени и описания задачи
     *
     * @param name        имя задачи
     * @param description описание задачи
     */
    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }

    /**
     * Получение идентификатора экземпляра
     *
     * @return Идентификатор экземпляра задачи
     */
    public Long getId() {
        return id;
    }

    /**
     * Получение имени задачи
     *
     * @return имя задачи
     */
    public String getName() {
        return name;
    }

    /**
     * Получение описания задачи
     *
     * @return описание задачи
     */
    public String getDescription() {
        return description;
    }

    /**
     * Задание имени проекта
     *
     * @param name новое имя задачи
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Задание описания задачи
     *
     * @param description описание задачи
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Получение идентификатора проекта
     *
     * @return идентификатор проекта
     */
    public Long getProjectId() {
        return projectId;
    }

    /**
     * Задание идентификатора проекта
     *
     * @param projectId идентификатор проекта
     */
    public void setProjectId(final Long projectId, final String projectName) {
        this.projectId = projectId;
        this.projectName = projectName;
    }

    /**
     * Получение текстового представления данных проекта
     *
     * @return текстовое представление данных проекта
     */
    @Override
    public String toString() {
        return id + ": " + name + " (" + description + ")" + (projectName != null ? " <" + projectName + ">" : "");
    }

}
