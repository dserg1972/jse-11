package com.nlmk.dezhemesov.taskmanager.entity;

import com.nlmk.dezhemesov.taskmanager.enumerate.Role;
import com.nlmk.dezhemesov.taskmanager.util.Hash;

/**
 * Сущность Пользователь
 */
public class User {

    ;

    /**
     * Идентификатор
     */
    private final Long id = System.nanoTime();
    /**
     * Имя
     */
    private String name = "";
    /**
     * Фамилия
     */
    private String surname = "";
    /**
     * Отчество
     */
    private String secondName = "";
    /**
     * Роль
     */
    private Role role = Role.USER;
    /**
     * Имя в системе
     */
    private String login;
    /**
     * Хэш пароля
     */
    private String passwordDigest = "";

    /**
     * Конструктор
     *
     * @param login имя учётной записи
     */
    public User(String login) {
        this.login = login;
    }

    /**
     * Конструктор
     *  @param login      имя учетной записи
     * @param password   пароль
     * @param role       роль
     * @param surname    фамилия
     * @param name       имя
     * @param secondName отчество
     */
    public User(String login, String password, Role role, String surname, String name, String secondName) {
        this.name = name;
        this.surname = surname;
        this.secondName = secondName;
        this.role = role;
        this.login = login;
        this.passwordDigest = Hash.generateMD5(passwordDigest);
    }

    /**
     * получение идентификатора
     *
     * @return идентификатор
     */
    public Long getId() {
        return id;
    }

    /**
     * Получение имения
     *
     * @return имя пользователя
     */
    public String getName() {
        return name;
    }

    /**
     * Установка имени пользователя
     *
     * @param name имя пользователя
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Получение фамилии
     *
     * @return фамилия
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Установка фамилии
     *
     * @param surname фамилия
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Получение отчества
     *
     * @return отчество
     */
    public String getSecondName() {
        return secondName;
    }

    /**
     * Установка отчества
     *
     * @param secondName отчество
     */
    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    /**
     * Получение роли
     *
     * @return роль
     */
    public Role getRole() {
        return role;
    }

    /**
     * Установка роли
     *
     * @param role роль
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * Получение имени учётной записи
     *
     * @return имя учётной записи
     */
    public String getLogin() {
        return login;
    }

    /**
     * Получение хэша пароля
     *
     * @return хэш пароля
     */
    public String getPasswordDigest() {
        return passwordDigest;
    }

    /**
     * Установка пароля
     *
     * @param password пароль
     */
    public void setPassword(String password) {
        passwordDigest = Hash.generateMD5(password);
    }

    /**
     * Получение информации о пользоватете в виде строки
     *
     * @return строка
     */
    @Override
    public String toString() {
        return id + ": " + login + " [" + role.name() + "] (" + surname + " " + name + " " + secondName + ")";
    }


    public static void main(String[] args) {
        User user = new User("admin");
        user.setPassword("123456");

        System.out.println(user.getPasswordDigest());
    }

}
