package com.nlmk.dezhemesov.taskmanager.repository;

import com.nlmk.dezhemesov.taskmanager.entity.User;
import com.nlmk.dezhemesov.taskmanager.enumerate.Role;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Репозиторий пользователей
 */
public class UserRepository {

    /**
     * Исключение: Дубликат пользователя
     */
    public static class DuplicateUserException extends Exception {
    }

    /**
     * Хранилище подьзователей
     */
    private List<User> users = new ArrayList<>();

    {
        try {
            create("admin", "admin", Role.ADMIN);
            create("user", "user", Role.USER);
        } catch (DuplicateUserException e) {
            Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, "Duplicate login!", e);
        }
    }

    /**
     * Создание пользователя и добавление в репозиторий
     *
     * @param login    логин
     * @param password пароль
     * @return созданный пользователь
     * @throws DuplicateUserException при попытке создать пользователя с таким же login
     */
    public User create(String login, String password, Role role) throws DuplicateUserException {
        User findUser = users.stream().filter(u -> u.getLogin().equals(login)).findFirst().orElse(null);
        if (findUser != null)
            throw new DuplicateUserException();
        User user = new User(login);
        users.add(user);
        user.setPassword(password);
        user.setRole(role);
        return user;
    }

    /**
     * Поиск пользователя в репозитории
     *
     * @param login логин
     * @return найденный пользователь
     */
    public User find(String login) {
        User findUser = users.stream().filter(u -> u.getLogin().equals(login)).findFirst().orElse(null);
        if (findUser == null)
            return null;
        return findUser;
    }

    public List<User> findAll() {
        return users;
    }


}
